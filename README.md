# ArduinoLed G2

El proyecto consiste en la implementación de un semáforo en Arduino. Se usará un Arduino Uno junto con 3 LEDS: Rojo, Amarillo y Verde. Además contará con un botón para ir alternando entre cada color.

Los miembros del proyecto y sus tareas:

    -Jesus Jimenez: Código de Arduino
    -Javier Iglesias: Esquemáticos